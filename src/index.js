import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import reducer from './store/reducer';
import thunk from "redux-thunk";

const store = createStore(reducer, applyMiddleware(thunk))

const app = (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(<BrowserRouter>{app}</BrowserRouter>,document.getElementById('root'));
