import {FETCH_POSTS_FAILURE, FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS, FETCH_POST_BY_ID} from "./action";

const initialState = {
    posts: null,
    postById: null,
    loading: true,
}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_POST_BY_ID:
            return {...state, postById: action.post}
        case FETCH_POSTS_REQUEST:
            return {...state, loading: true};
        case FETCH_POSTS_SUCCESS:
            return {...state, loading: false, posts: action.posts};
        case FETCH_POSTS_FAILURE:
            return {...state, loading: false};
        default:
            return state;
    }
}

export default reducer;