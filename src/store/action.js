import axiosPosts from "../axios-posts";


export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const FETCH_POST_BY_ID = 'POST_BY_ID';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostsFailure = posts => ({type: FETCH_POSTS_FAILURE});

const fetchPostByIdSuccess = post => ({type: FETCH_POST_BY_ID, post})


export const fetchPosts = () => {
    return async (dispatch) => {
        dispatch(fetchPostsRequest());
        try {
            const response  = await axiosPosts.get('/posts.json');
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
        }
    };
};

export const handleSubmitForm = (data, history) => {
    return async (dispatch) => {
        try {
            await axiosPosts.post('/posts.json', data);
            history.push('/')
        }  catch (e) {
            dispatch(fetchPostsFailure());
        }
    }
}

export const handleUpdateForm = (data, history, id) => {
    return async (dispatch) => {
        try {
            await axiosPosts.put('/posts/' + id + '.json', data);
            history.push('/');
        }  catch (e) {
            dispatch(fetchPostsFailure());
        }
    }
}

export const handleFetchSingleObjectToUpdate = (id) => {
    return async (dispatch) => {
        dispatch(fetchPostsRequest());
        try {
            const postResponse = await axiosPosts.get('/posts/' + id + '.json');
            dispatch(fetchPostByIdSuccess(postResponse.data))
        }  catch (e) {
            dispatch(fetchPostsFailure());
        }
    }
}

export const handleDeleteSingleObject = (id, history) => {
    return async (dispatch) => {
        try {
            await axiosPosts.delete('/posts/' + id + '.json');
            history.push('/');
        }  catch (e) {
            dispatch(fetchPostsFailure());
        }
    }
}

