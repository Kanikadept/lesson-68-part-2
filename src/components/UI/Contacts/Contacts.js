import React from 'react';
import './Contacts.css';

const Contacts = () => {
    return (
        <div className="contacts">
            <div className="row">
                <label>Email: </label>
                <p>Attractor@gmail.com</p>
            </div>
            <div className="row">
                <label>About Us</label>
                <p>Phone number: 0210302130</p>
            </div>
        </div>
    );
};

export default Contacts;