import React from 'react';
import './AboutUs.css';

const AboutUs = () => {
    return (
        <div className="about-us">
            <div className="row">
                <label>About Us</label>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae facilis itaque suscipit tempore? Aperiam atque commodi doloremque error maxime natus nihil obcaecati provident quo quod? Odit optio repellendus soluta unde.</p>
            </div>
            <div className="row">
                <label>Address: </label>
                <p>Sovetskaya 2482</p>
            </div>
        </div>
    );
};

export default AboutUs;