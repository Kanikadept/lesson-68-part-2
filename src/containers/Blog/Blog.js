import React, {useEffect} from 'react';
import './Blog.css';
import Post from "../Post/Post";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/action";

const Blog = props => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.posts);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch])

    return (
        <div className="blog">
            {state && Object.keys(state).map((key, index) => {
                return (
                    <Post id={key}
                          key={key}
                          time={state[key].dateTime}
                          title={state[key].title}
                          description={state[key].description}
                    />
                )
            })}
        </div>
    );
};

export default Blog;