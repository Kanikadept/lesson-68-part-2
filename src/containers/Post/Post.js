import React from 'react';
import {NavLink} from 'react-router-dom';
import './Post.css';

const Post = props => {

    return (
        <div className="post">
            <p>{props.time}</p>
            <p>{props.title}</p>
            <button><NavLink to={`/posts/${props.id}`}>Read More >></NavLink></button>
        </div>
    );
};

export default Post;