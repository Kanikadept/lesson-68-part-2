import React, {useState} from 'react';
import './PostForm.css';
import {handleSubmitForm} from "../../../store/action";
import {useDispatch} from "react-redux";


const PostForm = props => {
    const [post, setPost] = useState({
        title: '',
        description: '',
        dateTime: ''
    })
    const dispatch = useDispatch();

    const changeHandler = (e) => {
        const copy = {...post};
        copy[e.target.name] = e.target.value;
        setPost(copy);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            title: post.title,
            description: post.description,
            dateTime: new Date()
        }
        dispatch(handleSubmitForm(data, props.history));
    }

    return (
        <div className="post-form">
            <p>Add new post</p>
            <form onSubmit={handleSubmit}>
                <label>Title</label>
                <input value={post.title} onChange={changeHandler} name="title" type="text" required/>
                <label className="description-lab">Description</label>
                <textarea value={post.description} onChange={changeHandler} name="description" required/>
                <button>Save</button>
            </form>
        </div>
    );
};

export default PostForm;