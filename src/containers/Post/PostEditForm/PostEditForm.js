import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {handleUpdateForm} from "../../../store/action";
import './PostEditForm.css';


const PostEditForm = props => {
    const [formData, setFormData] = useState({})
    const postById = useSelector(state => state.postById)
    const dispatch = useDispatch();

    useEffect(() => {
        setFormData(postById)
    }, [postById])

    const handleFormDataChange = (event) => {
        const {name, value} = event.target;
        setFormData(formData => ({
            ...formData,
            [name]: value, dateTime: new Date().toLocaleString()
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        dispatch(handleUpdateForm(formData, props.history, props.match.params.id));
    }

    return (
        formData && <div className="post-edit-form">
            <p>Edit post</p>
            <form onSubmit={handleSubmit}>
                <label>Title</label>
                <input value={formData.title || ''} onChange={handleFormDataChange} name="title" type="text" required/>

                <label className="description-lab">Description</label>
                <textarea value={formData.description || ''} onChange={handleFormDataChange} name="description" required/>
                <button>Edit</button>
            </form>
        </div>
    );
};

export default PostEditForm;