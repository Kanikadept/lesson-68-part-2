import React, {useEffect, useState} from 'react';
import './SinglePost.css';
import {useDispatch, useSelector} from "react-redux";
import {handleDeleteSingleObject, handleFetchSingleObjectToUpdate} from "../../../store/action";


const SinglePost = props => {

    const [singlePost, setSinglePost] = useState(null);
    const postById = useSelector(state => state.postById)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(handleFetchSingleObjectToUpdate(props.match.params.id));
    }, [dispatch, props.match.params.id])

    useEffect(() => {
        setSinglePost(postById);
    }, [postById])

    const handleDelete = () => {
        dispatch(handleDeleteSingleObject(props.match.params.id, props.history));
    }

    const handleEdit = () => {
        props.history.push('/posts/'+ props.match.params.id +'/edit');
    }

    return (
        singlePost && <div className="single-post">
            <div className="content">
                <p>{singlePost.dateTime}</p>
                <p>{singlePost.title}</p>
                <p>{singlePost.description}</p>
            </div>
            <div className="btns">
                <button onClick={handleEdit}>Edit</button>
                <button onClick={handleDelete}>Delete</button>
            </div>
        </div>
    );
};

export default SinglePost;